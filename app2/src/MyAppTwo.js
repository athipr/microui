import ReactDOM from 'react-dom';
import React from 'react';
import AppTwo from './components/AppTwo';

export default class MyAppTwo extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    ReactDOM.render(<AppTwo />, this)
  }
}

customElements.define('my-app-two', MyAppTwo);

