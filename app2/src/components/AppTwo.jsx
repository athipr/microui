import React, { Component } from 'react';

class AppTwo extends Component {
  render() {
    return (
      <div>
        <h2>Hello, AppTwo</h2>
      </div>
    );
  }
}

export default AppTwo;
