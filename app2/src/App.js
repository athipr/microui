import React from 'react';
import logo from './logo.svg';
import './App.css';
import AppTwo from './components/AppTwo';

function App() {
  return (
    <div className="App">
      <AppTwo />
    </div>
  );
}

export default App;
