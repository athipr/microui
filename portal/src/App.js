import React from 'react';
import { Route, Switch } from 'react-router-dom'
import './App.css';

import One from './components/One';
import Two from './components/Two';
import NotFound from './components/NotFound';

function App() {
  return (
    <div>
    <Switch>
         <Route exact={true} path="/" component={One}/>
         <Route exact={true} path="/One" component={One}/>
         <Route exact={true} path="/Two" component={Two}/>
         <Route component={NotFound}/>
    </Switch>
    </div>
  );
}

export default App;
