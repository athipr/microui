import React, { Component } from 'react';

class Two extends Component {
  componentWillMount() {
    const script = document.createElement("script");
    script.async = true;
    script.src = 'http://localhost:3002/static/js/main.js';
    document.body.appendChild(script);
  }

  render() {
    return (
      <div>
        <my-app-two />
      </div>
    );
  }
}

export default Two;
