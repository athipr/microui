import React, { Component } from 'react';

class One extends Component {
  componentWillMount() {
    const script = document.createElement("script");
    script.async = true;
    script.src = 'http://localhost:3001/static/js/main.js';
    document.body.appendChild(script);
  }

  render() {
    return (
      <div>
        <my-app-one />
      </div>
    );
  }
}

export default One;
