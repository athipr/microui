import React from 'react';
import logo from './logo.svg';
import './App.css';
import AppOne from './components/AppOne';

function App() {
  return (
    <div className="App">
      <AppOne />
    </div>
  );
}

export default App;
