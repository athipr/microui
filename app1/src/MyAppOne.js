import ReactDOM from 'react-dom';
import React from 'react';
import AppOne from './components/AppOne';

export default class MyAppOne extends HTMLElement {
  constructor() {
    super();
  }

  connectedCallback() {
    ReactDOM.render(<AppOne />, this)
  }
}

customElements.define('my-app-one', MyAppOne);
